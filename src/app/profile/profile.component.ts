import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from '@angular/router';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user:{  firstname: string,
  lastname: string,
  username: string,
  password:string

  }

  constructor(private http: HttpClient, private router: Router) { }
	private subscriber: any;
  ngOnInit() {
    if(localStorage.length>0) {
      const User = localStorage.getItem('currentUser');
      //console.log(User);
      const headers = new HttpHeaders({Authorization: `Bearer ${User}`});
      this.http.get('http://192.168.5.60:4200/api/users/current', {headers})
      //this.http.get('http://192.168.5.171:8000/api/users/current', {headers})
      .subscribe((data: any) => {
        //console.log(data);
        document.getElementById('show').innerHTML = 'Username: ' + data.username + '<br>First Name: ' + data.firstName +
        '<br>Last Name: ' + data.lastName + '<br>Created on: ' + data.createdDate;
      }, error => {
        console.log(error.error.message);
      });
    }
    else {
      this.router.navigate(['login']);
    }
  }
}
