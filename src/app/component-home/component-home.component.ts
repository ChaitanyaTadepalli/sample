import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http'
import { ViewportScroller } from '@angular/common';
import {Igetpost} from '../getpost';
import { Observable } from 'rxjs';
import {Router} from '@angular/router';
import { $ } from 'protractor';


@Component({
  selector: 'app-component-home',
  templateUrl: './component-home.component.html',
  styleUrls: ['./component-home.component.css']
})
export class ComponentHomeComponent implements OnInit {
  public post = [];

  //info= td.getAttribute('postId');
  
  constructor(private http:HttpClient , private router :Router) { 
    
  }

  ngOnInit() {

    this.getPost().subscribe(data => this.post = data);
    

  }
  getPost() : Observable<Igetpost[]>{

    const User = localStorage.getItem('currentUser');
    const headers = new HttpHeaders({Authorization: `Bearer ${User}`});
    return this.http.get<Igetpost[]>('http://192.168.5.60:4200/api/users/posts/viewPosts',{headers});
    //return this.http.get<Igetpost[]>('http://192.168.5.171:8000/api/users/posts/viewposts',{headers});
    
  }

  viewPost(item){
    console.log("posting..."+item.postId);
    /*const x = document.getElementById('get');
    console.log(x);*/
    const User = localStorage.getItem('currentUser');
    //console.log(User);
    const headers = new HttpHeaders({Authorization: `Bearer ${User}`});
    const body = {postId:item.postId};
    console.log(body);
    //this.http.post('http://192.168.5.171:8000/api/users/posts/getpost', body, {headers})
    this.http.post('http://192.168.5.60:4200/api/users/posts/getPost', body, {headers})
    .subscribe((data: any) => {
    //alert("successfully register");
    console.log(data);
    this.router.navigate(['register']);
    },error => {
      console.log(error.error.message);
    });
  }
}
