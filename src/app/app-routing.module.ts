import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentLoginComponent } from './component-login/component-login.component';
import { ComponentHomeComponent } from './component-home/component-home.component';
import { ComponentRegisterComponent } from './component-register/component-register.component';
import {ProfileComponent } from './profile/profile.component';
import { LogoutComponent } from './logout/logout.component';



const routes: Routes = [
  { path: '',component:  ComponentHomeComponent},

  {path: 'login',component:  ComponentLoginComponent},
  {path: 'register',component: ComponentRegisterComponent},
  {path: 'profile',component:ProfileComponent},
  {path: 'logout', component: LogoutComponent},

{ path: '**',redirectTo:''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
